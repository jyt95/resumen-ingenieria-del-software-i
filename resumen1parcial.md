# Resumen 1er Parcial

## Introducción

### (!!!) Costo, Planificación, Productividad y Calidad

Las tres fuerzas básicas que guian el desarrollo del sw son:

- Costo: El costo debe ser razonable
- Planificación: El tiempo de producción debe ser razonable
- Calidad: La calidad debería ser alta

#### Costo
El principal costo de producción es la mano de obra empleada.
El sw es muy caro. De hecho el sw se ha encarecido al punto de superar al costo
relativo del hw (en particular el mantenimiento es muy caro).

#### Planificación
Muchos proyectos son desbocados (runaway). Los tiempos del sw deberían ser
disminuídos.

#### Productividad: Una medidad de Costo y Planificación
La productividad medida en KLOC por persona/mes captura tanto el costo como
la planificación.

#### (!!!) Calidad
La calidad es difícil de definir, aún así se pueden reconocer 6 dimensiones
para evaluarla.
Son: (**FunManUsaEfiPorCon**)

- **Funcionalidad:** Capacidad de proveer funciones que cumplen las
necesidades establecidas o implicadas.

- **Confiabilidad:** Capacidad de realizar las funciones requeridas
bajo las condiciones establecidas durante un tiempo especifico.

- **Usabilidad:** Capacidad de ser comprendido, aprendido y usado.

- **Eficiencia:** Capacidad de proveer desempeño apropiado relativo
a la cantidad de recursos.

- **Portabilidad:** Capacidad de ser adaptado a distintos entornos
sin aplicar otras acciones que las provistas a este proposito en el producto.

- **Mantenibilidad:** Capacidad de ser modificado con el proposito de
corregir, mejorar o adaptar.

    - Correcciones (updates)
    - Mejoras o adaptaciones al cambio (upgrades)

<div style="page-break-after: always;"></div>

### Escala y Cambio
#### Escala
El desarrollo de un sistema grande de sw implica el uso de otros métodos
distintos a los necesarios para desarrollar un sistema chico de sw. Tanto los
métodos de ingeniería como los métodos de administración deben ser formales.

#### Cambio
Es normal que el conjunto de requerimientos cambie durante el desarrollo del sw.
Por lo tanto es necesario disponer de métodos para el desarrollo que se adapten
a cambios eficientemente.

### Objetivo de la Ingeniería del Software

- La sucesiva producción de sistemas de alta calidad y alta productividad para
  problemas a gran escala que se adapten a cambios.

### (!!!) Fases del proceso de desarrollo
Los enfoques sistematicos requieren que cada etapa se realice rigurosa
y formalmente.
Las 5 etapas son:

- Análisis y Especificación
- Arquitectura y Diseño
- Coding
- Testing
- Entrega e instalación

El proceso de desarrollo no establece como asignar los recursos a las distintas
tareas ni como organizarlas temporalmente, ni como asegurar que cada fase se
desarrolle apropiadamente, etc.

### (!!!) Costos de encontrar y resolver errores en las diferentes etapas del proyecto.
Los errores pueden ocurrir en cualquier fase durante el desarrollo.

El costo relativo de corregir un error originado en la fase de requerimientos
aumenta exponencialmente a medida que se van pasando a las siguientes fases. Si
hay un error en los requerimientos, el diseño y el código se verán afectados.
Para corregir el error tanto el diseño como el código deberán modificarse,
incrementado el costo de la corrección.

La detección de errores y su corrección deberían constituir un proceso a
realizar en todas las fases del desarrollo del sw.
En términos de las fases del desarrollo, esto implica que deberíamos tratar de
verificar el output de cada fase antes de empezar con la siguiente. En otras
palabras un proceso debería tener actividades de control de calidad durante el
proceso y en cada fase. Una actividad de control de calidad es una actividad tal
que su propósito principal es detectar y corregir errores.

### ¿Por qué el desarrollo se divide en fases? Divide y vencerás
El motivo de separar en fases es la separación de incumbencias: cada fase
manipula distintos aspectos del desarrollo de SW.
El proceso en fases permite verificar la calidad y el progreso en momentos
definidos del desarrollo.
Por otra partes, las fases se realizan en el orden especificado por el modelo
de proceso que se elija seguir.

<div style="page-break-after: always;"></div>

## (!!!) Análisis y especificación de los requisitos del software.

### (!!!) Diferencia entre cliente y usuario.
El **cliente** es aquel que propone el problema, contacta al desarrollador (o
analista) para indicar las condiciones que se deben cumplir en el sistema.

El **usuario** es quien, efectivamente, utilizará el sistema.

#### Problemas de comunicación posibles entre cliente, usuario y desarrollador.
El cliente, muchas veces, no comprende el proceso de desarrollo del sw. Incluso,
suele ocurrir que no sabe exactamente qué es lo que necesita que el sistema
haga.

Por otro lado, el desarrollador no conoce el problema del cliente ni su area de
aplicación.

### Definición de requerimientos
- Una condición o capacidad necesaria por un usuario para solucionar un problema
  o alcanzar un objetivo.
- Una condición o capacidad necesaria que debe poseer o cumplir un sistema para
  satisfacer un contrato, un estándar u otro documento formal.

### Ventajas de una SRS
- Establece las bases del acuerdo entre el cliente y el proveedor de lo que el
  sw hará.
- Una SRS provee una referencia para la validación del producto final.
- Una SRS de alta calidad es prerequisito para un sw de alta calidad.
- Una SRS de alta calidad reduce el costo de desarrollo

### (!!!) Análisis, especificación y validación

- El **analisis** es la etapa en la que se entiende el problema; se realizan
  entrevistas con el cliente, lectura de manuales, etc. Se hacen gráficos
  descriptivos del problema para verificar que tanto el desarrollador como el
  cliente/usuario lo entiendan. El análisis se enfoca en la estructura del
  problema.

  Las estructuras de análisis detalladas y completas no son críticas.
  El principal objetivo de las estructuras de análisis es ayudar en la
  representación de la información y la partición del problema, por lo que las
  estructuras no son un fin en sí mismo.

- En la **especificación** se realiza una aproximación más formal del problema.
  Se quitan las ambiguedades, redundancias, etc. La especificación se enfoca en
  el comportamiento externo. En esta etapa se realiza la SRS.

  UI, situaciones excepcionales, restricciones de rendimiento, restricciones
  de diseño, seguimiento de estándares, recuperación, etc. sólo adquieren
  importancia en la etapa de especificación de los requirimientos.

- La **validación** se concentra en asegurar que lo especificado en la SRS son
  de hecho todos los requerimientos del sw y asegurar que la SRS sea de alta
  calidad.

Luego de estos pasos el proceso termina con una **SRS validada**.

<div style="page-break-after: always;"></div>

### (!!!) Características de la SRS (CoCoNoCoVRaMO)

- **Correcta**: Cada requerimento representa precisamente alguna caracteristica
deseada en el sistema final.

- **Completa**: Todo lo que se supone que debe hacer el sw y las respuestas del sw a todas las clases de input de datos están en la SRS.

- **No ambigua**: Cada requerimiento tiene exactamente una única interpretación
   (no se superponen con otro).

- **Consistente**: Ningun requerimento contradice a otro.

- **Verificable**: Si existe algun proceso costo efectivo que puede verificar
  que el SW final satisface el requerimiento.

- **Rastreable**: Se debe poder determinar el origen de cada requerimiento y
  cómo este se relaciona a los elementos del software.

  - **Hacia delante**: Dado un requerimiento se debe poder detectar en qué
    elementos del diseño o del código tienen impacto.
  - **Hacia atrás**: Dado un elemento de diseño o código se debe poder rastrear
    qué requerimientos está atendiendo.

- **Modificable**: Si la estructura y estilo de la SRS es tal que permite
  incorporar cambios facilmente preservando completitud y consistencia. La
  redundancia es un gran estorbo (puede causar inconsistencias).

- **Ordenada en aspectos de importancia y estabilidad**: Los requerimientos
  pueden ser criticos, importantes pero no criticos, deseables pero no
  importantes. Similarmente, algunos requerimientos forman un núcleo que no
  cambian con el tiempo mientras que otros sí.

### Componentes de una SRS

- **Funcionalidad**: Los requisitos funcionales especifican qué se espera del comportamiento del sistema, qué outputs se producen dados cientros inputs. La descripción debe ser detallada (pero se debe dar el qué, no el cómo), incluídos los casos excepcionales.

- **Performance**: Son partes de la SRS que especifican las restricciones de performace en el software.

    - **Requisitos Estáticos o de Capacidad**: No imponen restricciones en las características de la ejecución del sistema. Incluyen requerimientos como el número de terminales a ser soportadas, el número de usuarios en paralelo.

    - **Requisitos Dinámicos**: Especifican restricciones en la ejecución del comportamiento del sistema, estos típicamente incluyen el tiempo de respuesta y restricciones de rendimiento en el sistema. Caracterizarlos en términos medibles.

- **Restricciones de Diseño impuestas en la implementación**: Factores del ambiente del cliente que restringen las posibilidades de diseño.

    - **Cumplir estándares**
    - **Limitaciones de Hardware**
    - **Confiabilidad y tolerancia a fallos**
    - **Seguridad**

- **Interfaces Externas**: Todas las interacciones del software con la gente,
  hardware y otro software tienen que estar claramente especificado.



<div style="page-break-after: always;"></div>

### (!!!) Modelado DFD
El DFD captura la manera en que ocurre la transformacion de la entrada en la
salida a medida que los datos se mueven a traves de los transformadores.
La transformacion se realiza a traves de **"Transformadores"**.

Este diagrama consta de:

- **Transformadores de datos** ->  burbujas.
- **"Pipes" por donde fluyen los datos** -> flechas
- **Productores/consumidores de datos** (usualmente fuera del sistema) ->
rectangulos.
- **Archivos externos** -> linea recta etiquetada.
- Múltiples flujos de datos (significa '**and**') -> asterisco (*)
- Relación '**or**' -> más (+)


### (!!!) Diccionario de datos: especifican más la flecha del DFD

- El diccionario de datos define con mayor precisión los datos en un DFD.
- Muestra la estructura de los datos; la estructura se hace más visible a medida
  que se van explotando las burbujas.
- Se puede usar expresiones regulares para representar la estructura de datos.

Ejemplo:
```
    pay_rate = [Hourly | Daily | Weekly]  + Dollar_amount

    Employee_name = Last + First + Middle_initial
```
<div style="page-break-after: always;"></div>

### (!!!) **Prototipado**

**CLAVE**: Cliente, usuarios y desarrolladores lo utilizan para comprender mejor
el problema y las necesidades.
Si el problema está entendido, ni siquiera hago un prototipo.

Existen dos tipos de prototipos:

- **Descartable:** el prototipo se construye con la idea de desecharlo luego de
culminado la fase de requerimentos.
- **Evolucionario:** se construye con la idea de que evolucionara al sistema
final.

¿Por qué se recomienda usarlo y tirarlo?<br>
Porque se suele hacer muy rápido, sucio, sin preocuparse por la calidad.

### (!!!) Método de análisis estructurado

Pasos principales de este método:

1) Dibujar el diagrama de contexto.

   **Diagrama de contexto**: Ve al sistema como un transformador e identifica
   el contexto. Es un DFD con un único transformador (el sistema).

2) Dibujar el DFD del sistema existente
   - Objetivo: comprender funcionamiento
   - Se puede usar jerarquía
   - Cada burbuja -> transformación lógica de algunos datos
   - Para obtenerlo se debe interactuar intensamente con el usuario
   - Se valida con los usuarios, haciendo "caminatas" por el DFD

3) Dibujar el sistema propuesto e identificar la frontera hombre-maquina.
   - El DFD debe modelar el sistema propuesto completo sin tener en cuenta
     si un proceso es automático o manual
   - Validar con el usuario
   - Establecer luego la frontera hombre-máquina => qué es manual, qué es
     automático
   - Mostrar interacción entre procesos manuales y procesos automáticos

### (!!!) Método orientado a objetos:
Se toma el *análisis* orientado a objetos. Importante decir claramente el nombre
de la clase; especificar la simbología empleada para mostrar las relaciones
entre clases (M2M, O2M, ...). Prestar atención a las cosas pedidas por la
consigna.

<div style="page-break-after: always;"></div>

### (!!!) Casos de uso

**Casos de Uso**: captura el comportamiento del sistema como interacción de los
usuarios con el sistema. Captura un contrato entre el usuario y el
comportamiento del sistema.

- Se enfoca sólo en la especificación de la funcionalidad (comportamiento
  externo). Forman sólo la parte funcional de la SRS.
- Un caso de uso es una colección de muchos escenarios. Un escenario puede
  emplear otros casos de usos en un paso (jerarquía de los casos de usos).
- La forma básica es textual.
- Puede utilizarse también durante análisis.
- Útil en la recolección de requerimientos dado que a los usuarios les agrada y
  comprenden el formato y reaccionan a este fácilmente.
- Muy adecuado para sistemas interactivos.

NOTA: Importante: numerar, decir quién es el actor principal, precondiciones,
escenarios exitosos y fallas bien numeradas. Usar oraciones gramaticalmente
sencillas.

### ¿A qué se llama "Actor Principal" del caso de uso?
Un **Actor** es una persona o un sistema que interactua con el sistema propuesto
para alcanzar un objetivo.
Un actor es una entidad logica; actores receptores y actores transmisores son
distintos (aún si es el mismo individuo).

Un **Actor Primario** (o actor principal) es el que inicia el caso de uso.
El caso de uso debe satisfacer su objetivo. La ejecucion real puede ser realizada
por un sistema y otra persona en representacion del actor primario.

### ¿Qué es un escenario?
Es un conjunto de acciones realizadas con el fin de alcanzar un objetivo bajo
determinadas condiciones.
Las acciones se especifican como un conjunto de pasos que generalmente se
representan en secuencia, pero no necesariamente esa es su implementacion.
Un paso es una accion logicamente comple realizada tanto por el actor como por el
sistema.
Es una interaccion entre el usuario y el sistema.
Existen dos grupos de escenarios:

- **Escenario Exitoso Principal**: Cuando todo funciona normalmente y se alcanza
el objetivo.
- **Escenarios Alternativos** (de excepcion): cuando algo sale mal y el objetivo
no puede ser alcanzado.

<div style="page-break-after: always;"></div>

### (!!!) ¿Cuáles son los errores típicos detectados a partir de la Validacion?
Debido a la naturaleza de la etapa de Analisis y Especificacion, hay muchas posibilidades de malentendidos.
Es caro corregir los defectos de requerimientos mas tarde.
Los errores mas comunes son:
- Omision (30%)
- Inconsistencia (10-30%)
- Hechos incorrectos (10-30%)
- Ambiguedad (5-20%)

### Otras características de la Validación

- La revisión de la SRS la realizan el autor, el cliente, los representantes de
  los usuarios y los desarrolladores.
- Se pueden detectar entre 40% y 80% de los errores de requerimientos.
  Las listas de control son muy útiles para ello.

- Hay lenguajes de especificación formal acompañados de herramientas
  automáticas que sirven para verificar consistencia, dependencias circulares o
  propiedades específicas. También permiten simular para poder comprender completitud y corrección.

<div style="page-break-after: always;"></div>


### (!!!) Métricas: "...de Punto Punción sepansé 5."
El punto función define el **tamaño** en terminos de la **funcionalidad**.
Los tipos de funciones son:

- **Entradas externas:** Tipo de entrada externa a la aplicación
- **Salidas externas:** Tipo de salida que deja el sistema
- **Archivos logicos internos:** Grupo logico de dato/control de información/
usado/manipulado.
- **Archivos de interfaz eterna:** Archivos compartidos entre aplicaciones.
- **Transacciones externas:** Input/Output inmediatos.<br>

Se debe contar cada tipo de función diferenciando según sea compleja, promedio o
simple.
C<sub>ij</sub> denota la cantidad de funciones tipo **"i"** con complejidad **"j"**

**Punto Función no Ajustado**:<br>
**UFP** = sum(i=1 to 5, sum(j = 1 to 3, w<sub>ij</sub>C<sub>ij</sub>))

**Factor de Ajuste de Complejidad**:<br>
**CAF** = 0.65 + 0.01 sum(i = 0 to 14, p<sub>i</sub>)

**Punto Función**:<br>
**FP** = CAF * UFP

Ajustar el UFP de acuerdo a la complejidad del entorno. Se evalúa segun las
siguientes caracteristicas:

1) Comunicación de Datos
2) Intención de facilitar cambios
3) Objetivos de despempeño
4) Reusabilidad
5) Complejidad del procesamiento lógico.

Cada uno de estos items debe evaluarse como:

- no presente                 ->  0
- Influencia insignificante   ->  1
- influencia moderada         ->  2
- influencia promedio         ->  3
- influencia significativa    ->  4
- influencia fuerte           ->  5

1 Punto Función =

- 125 LOC en C
- 60 LOC en C++ o Java

### Métricas de calidad

- **Metricas de calidad directa**: Evalúan la calidad del documento estimando
  el valor de los atributos de calidad de la SRS.
- **Metricas de calidad indirecta**: Evalúan la efectividad de las métricas del control de calidad usadas en el proceso en la fase de requerimientos. Para que funcione el proceso debe estar bajo control estadístico. Ej: Número de errores encontrados y frecuencia de cambios de requerimientos.

<div style="page-break-after: always;"></div>

## (!!!) Arquitectura
Es la estructura del sistema que comprende los elementos del SW, las propiedades
externamente visibles de tales elementos y la relacion entre ellas.

-  Solo interesan las propiedades externas necesarias para especificar las relaciones.
- No son importantes los detalles de como se aseguran dichas propiedades.

La arquitectura es el diseño del sistema al mas alto nivel.
A este nivel se hacen las elecciones de:

- Tecnologia
- Productos a utilizar
- Servidores
- etc...

Es la etapa mas temprana donde algunas de propiedades como la confiabilidad y
desempeño pueden evaluarse.

### ¿Por qué una descripción Arquitectónica? (Ventajas)

- **Comprensión y comunicación**: Al ocultar la complejidad de sus partes facilita la comunicación entre los distintos interesados (usuarios, cliente, arquitecto, diseñador, etc.) al crear un marco de comprensión común. Útil para negociaciones. Ayuda a comprender el sistema existente.
- **Reuso**: Se elige una arquitectura tal que las componentes existentes encajen adecuadamente con otras componentes a desarrollar. Qué componentes usar es una decisión de arquitectura.
- **Construcción y evolución**: La división provista será usada para guiar el desarrollo del sistema. Ayuda a asignar equipos de trabajo (paralelizar). Ayuda a determinar el orden de la construcción de las partes. En caso de cambios ayuda a determinar qué componentes debe n cambiarse y cuál es el impacto de dichos cambios en otras componentes.
- **Análisis**: Algunos análisis estimados de confiabilidad y desempeño pueden hacerse en esta etapa. Esto permite pasar estimaciones a la fase de diseño.

### Definición de Vista
- Una vista consiste de elementos y relaciones entre ellos y describe una arquitectura. Los elementos de la vista dependen de lo que la vista quiera destacar. Facilita la comprensión al destacar ciertos elementos y no otros.

- Hay muchos tipos de vistas. Algunos comunes:

  - **Módulo**: Un sistema es una colección de unidades de código. Los elementos son módulos (Clases, paquetes, funciones, procedimientos, etc.). La relación entre ellos está basada en el código ("parte de", "usa a", "depende de", llamadas, generalización, etc.).
  - **Componentes y conectores**: Los elementos son entidades de ejecución denominados componentes (objetos, proces, .exe, .dll, etc.). Los conectores proveen el medio de interacción entre las componentes (pipes, sockets, memoria compartida, protocoles, etc.).
  - **Asignación de recursos**: Especifica la relación entre los elementos del sw y las unidades de ejecución en el entorno (hw, sistemas de archivos, gente, etc.). Exponen propiedades estructurales como qué proceso ejecuta en qué procesador, qué archivo reside dónde, etc.

<div style="page-break-after: always;"></div>

### Vista de componentes y conectores
**Componentes**: Elementos computaciones o de almacenamiento de datos.

  - Tiene un nombre que representa su rol
  - Tiene un tipo representado con un símbolo particular
  - Las componentes utilizan interfaces o puertos para comunicarse entre sí.

**Conectores**: Mecanismos de interacción entre las componentes.

  - Tiene un nombre que identifica la naturaleza de la interacción
  - Tiene un tipo que representa el tipo de interacción (aridad, direccionalidad)
  - Describen el medio en el cual la interacción toma lugar.
  - Un conector puede proveerse por medio del entorno de ejecución (llamada a procedimiento/función)


### (!!!) Detalles de la arquitectura Pipes & Filters, restricciones.

- Adecuado para sistemas que fundamentalmente hacen transformaciones de datos.
- Utiliza una red de transformadores para obtener el resultado deseado.
- **Componente:** filtro. Un filtro es una entidad independiente y asincrona.
- **Conector:** tubo. Canal unidireccional que transporta un flujo de datos entre filtros.

- **Restricciones:** Cada filtro debe trabajar sin conocer la identidad de los otros filtros. Un tubo debe conectar un puerto de salida a un puerto de entrada. Un sistema
puro requiere que cada filtro tenga su hilo.

### (!!!) Estilo Repositorio(vista de C&C)
Pertenece al estilo de datos compartidos.

- **Componentes:** repo de datos y usuarios de datos.
- Repo: provee de almacenamiento.
- Usuarios de datos: acceden a los datos, realizan calculos, y ponen los resultados en el repo.
- La comunicacion entre los usuarios se hace a traves del repo.
- **Conector:** lectura/escritura (las funcionalidades pueden ser distintas)

### (!!!) Estilo Cliente-Servidor, suele tener tres niveles(vista de C&C)

- **Componentes:** clientes y servidores.
- Los clientes se comunican con las servidores, pero no con otros clientes.
- La comunicacion siempre es iniciada por un cliente y espera respuesta del servidor.
- **Conector:** solicitud/respuesta.

<div style="page-break-after: always;"></div>

### Diferencias y Relaciones entre Arquitectura y Diseño
- La arquitectura **es un diseño** de muy alto nivel que se encuentra en el dominio de la solución y no en el dominio del problema.
- Lo que llamamos diseño se enfoca en los módulos que finalmente se transformarán en el código de tales componentes. **El diseño provee la vista de módulos del sistema.**
- La arquitectura sólo debe identificar las partes necesarias para evaluar las propiedades deseadas.
- La arquitectura no considera la estructura interna (archivos, estructuras de datos) pero puede imponer restricciones sobre el diseño (incluso sobre la implementación, siempre y cuando haya preservación de la integridad de la arquitectura).

### (!!!) ATAM

1) Recolectar escenarios:

- Los escenarios describen las interacciones del sistema.
- Elegir escenarios relevantes (exitosos y excepcionales).

2) Recolectar requerimientos y/o restricciones:

- Definir lo que se espera del sistema en tales escenarios.
- Especificar los niveles deseados para los atributos de interes.

3) Describir las vistas arquitectonicas

4) Analisis especificos a cada atributo:

- Se analizan las vistas bajo distintos escenarios separadamente para cada atributo de interes.
- Determina los niveles que la arquitectura puede proveer en cada atributo.
- Se comparan con los requeridos.
- Base para eleccion de una arquitectura u otra.

5) Identificar puntos sensitivos y de compromisos:

- Analisis de sens: cual es el impacto que tiene un elemento sobre un atributo de calidad. Los elementos de mayor impacto son los puntos de sens.
- Analisis de compromiso: Los puntos de compromiso son los puntos de sens para varios atributos.

<div style="page-break-after: always;"></div>

## (!!!) Diseño

- El diseño determina las mayores características de un sistema.
- Tiene un gran impacto en testing y mantenimiento.
- Comienza una vez que los requerimientos estén definidos, antes de implementar.
- Es el **desplazamiento del dominio del problema al dominio de la solución**.

### Niveles en el proceso de diseño

- **Diseño Arquitectónico**
- **Diseño de Alto Nivel**: Es la vista de módulos del sistema. Hay dos formas de hacerlo: Orientado a Objetos y Orientado a Funciones.
- **Diseño Detallado o Diseño Lógico**: Establece cómo se implementan las componentes/módulos de manera que satisfagan sus especificaciones. Incluye los detalles del procesamiento lógico y de las estructuras de datos.

### Criterios para Evaluar un diseño
Los criterios de evaluación suelen ser subjetivos y no cuantificables.

- **Corrección**: ¿El diseño implementa los requerimientos? ¿Es factible el diseño dadas las restricciones?
- **Eficiencia**: Le compete el uso apropiado de los recursos del sistema (principalmente CPU y memoria).
- **Simplicidad**: Al facilitar la comprensión tiene un impacto directo muy positivo en el mantenimiento, el testing y la codificación.

El diseño es creativo, no hay pasos, sólo principios:
- **Partición y Jerarquía**: Dividir el problema en pequeñas partes que sean manejables (deben poder modificarse y solucionarse separadamente). No es posible lograr total independencia y como la comunicación entre partes es compleja, es importante mantener la mayor independencia posible entre las partes para simplificar el diseño y facilitar el mantenimiento. El particionado del problema genera una jerarquía de componentes en el diseño.


- **Abstracción**: La abstracción de una componente describe el comportamiento externo sin dar detalles internos de cómo se produce este comportamiento.
  - **Abstracción Funcional**: Especifica el comportamiento funcional de un módulo.
  - **Abstracción de Datos**: Los datos se tratan como objetos junto a sus operaciones. Las operaciones definidas para un objeto sólo pueden realizarse sobre este objeto. Los detalles internos de los objetos permaneces ocultos y sólo sus operaciones son visibles.

- **Modularidad**: Un sistema se dice modular si consiste de componentes discretas tal que puedas implementarse separadamente y un cambio a una de ellas tenga mínimo impacto sobre las otras.
  - Provee la abstracción en el sw.
  - Es el soporte de la estructura jerárquica de los programas.
  - Mejora la claridad del diseño y facilita la implementación.
  - Reduce los costos de testing, debugging y mantenimiento.

<div style="page-break-after: always;"></div>

### Estrategias Top Down y Bottom Up
Un sistema es una jerarquía de componentes. Hay dos formas de diseñar tal jerarquía:
- **Top-Down**: Comienza en la componente de más alto nivel; prosigue construyendo las componentes de niveles más bajos descendiendo en la jerarquía.

  - Se especifica el sistema
  - Se define el módulo que implementará la especificación
  - Se especifica los módulos subordinados
  - Iterativamente se trata cada módulo subordinados como el nuevo problema
  - El refinamiento para cuando el módulo pueda ser implementado directamente
<br><br>
  - Ventajas:
    - En cada paso hay una clara imagen del diseño
    - Enfoque más natural
    - Muy usada
  - Desventajas:
    - La factibilidad es desconocida hasta el final

- **Bottom-Up**: Comienza por las componentes de más bajo nivel en la jerarquía; prosigue hacia niveles más altos.

<div style="page-break-after: always;"></div>

## (!!!) Diseño Orientado a Función

### Definición de Acoplamiento
Dos módulos no están acoplados (son independientes) si cada uno puede funcionar completamenta sin la precencia del otro. El acoplamiento es la dependencia entre dos módulos. Es una relación inter-modular, es independiente de la implementación.

Factores que influyen en el acoplamiento:
- Tipo de conexiones entre módulos (presencia de datos compartidos)
- Complejidad de las interfaces (presencia de parámetros innecesarios)
- Tipo de flujo de información entre módulos (presencia de parámetros de control)

Factores que disminuyen el acoplamiento:
- Sólo las entradas definidas en un módulo son utilizadas por otros
- La información se pasa exclusivamente a través de parámetros
- Baja cantidad de interfaces por módulo
- Evitar el uso de variables compartidas, el uso interfaces oscuras e indirectas, el uso directo de operaciones y atributos internos al módulo, la transferencia de información de control que hagan que las acciones de los módulos dependan de la información (más difícil de comprender).
- Evitar por completo la comunicación con información híbrida (pasar información de control y de datos) pues produce un alto acoplamiento.

### (!!!) Definición de Cohesión y Niveles de Cohesión
La cohesión entre los elementos de un módulo indica cuán relacionados están dichos elementos entre sí. Es una relación intra modular.
Usualmente, a mayor cohesión de los módulos, menor acoplamiento entre los módulos.

Niveles de cohesion, desde mas debil a mas fuerte (NO lineal):

- **Casual:** La relacion entre los elementos del modulo no tiene significado. (Un modulo el creado para evitar codigo repetido)
- **Logica:** Existe una relacion logica entre los elementos del modulo; realizan funcs dentro de la misma clase logica. (Un modulo que necesita un flag para saber que hacer)
- **Temporal:** Parecido al nivel anterior pero los elementos estan relacionados en el tiempo y se ejecutan juntos. (Inicializacion, finalizacion, ...)
- **Procedural:** Contiene elementos que pertenecen a una misma unidad procedural. (Un ciclo o secuencia de decisiones)
- **Comunicacional:** Existe una relacion por una referencia al mismo dato. (Pedir los datos de una cuenta personal y devolver todos los datos del registro)
- **Secuencial:** Los elementos estan juntos porque la salida de uno es la entrada del otro. (Secuencia de pasos para realizar una accion, buen acoplamiento y facil de mantener, malo para reuso)
- **Funcional:** Es la mas fuerte, todos los elementos estan relacionados para llevar a cabo UNA sola funcion. (Calcular el seno de un angulo)

<div style="page-break-after: always;"></div>

### Metodología de diseño estructurado: Introducción

- La estructura se decide durante el diseño.
- La implementación no debe cambiar la estructura.
- La estructura tiene efectos sobre el mantenimiento.
- SDM ve al sw como una función de transformación que convierte una
  entrada dada en la salida esperada.
- El foco en el diseño estructurado es la función de transformación. => SDM es
  una metodología orientada a funciones.

#### Objetivo de la SDM:
- Especificar módulos de funciones y sus conexiones siguiendo una
  estructura jerárquica con bajo acoplamiento y alta cohesión.

#### Idea:
- La mayoría de la computación se realiza en los módulos subordinados.
- El módulo principal se encarga de la coordinación.
- SDM apunta a acercarse a la factorización completa.

### Metodología de diseño estructurado 1: Reformular con DFDs
- El diseño estructurado comienza con un DFD que capture el flujo de
  datos del sistema propuesto.
- DFD es una representación importante: provee una visión de alto nivel
  del sistema.
- Enfatiza el flujo de datos a través del sistema.
- Ignora aspectos procedurales.


### (!!!) Metodología de diseño estructurado 2: Identificar MAI y MAO
**Entradas mas abstractas** (MAI):

- Elementos de datos en el DFD que estan mas distantes de la entrada real, pero aun puede considerarse como entrada.
- Son datos obtenidos luego del chequeo de errores, formateo, validacion, etc.
- Para encontrarla:
    1) Se debe ir desde la entrada fisica en direccion de la salida hasta que los datos no puedan considerarse entrantes.
    2) Ir lo mas lejos sin perder la naturaleza entrante.
- Es dual para obtener las **salidas mas abstractas** (MAO).

Este enfoque separa las distintas funciones:
- Subsistema que realiza principalmente la **entrada**.
- Subsistema que realiza principalmente las **transformaciones**.
- Subsistema que realiza principalmente la **presentación de la salida**.


### Metodología de diseño estructurado 3: Realizar el primer nivel de factorización
Primer paso para obtener el diagrama de estructura.
- Especificar el módulo principal.
- Especificar un módulo de entrada subordinado por cada ítem de dato
  de la MAI. El propósito de estos módulos de entrada es enviar al módulo
  principal los ítems de datos de la MAI.
- Especificar un módulo de salida subordinado por cada ítem de dato de
  la MAO.
- Especificar un módulo transformador subordinado por cada
  transformador central.
- Las entradas y salidas de estos módulos transformadores están
  especificadas en el DFD.

### Metodología de diseño estructurado 4.1: Factorizar los módulos de entrada

- El transformador que produce el dato de MAI se trata ahora como
  un transformador central.
- Se repite el proceso del primer nivel de factorización considerando al
  módulo de entrada como si fuera el módulo principal.
- Se crea un módulo subordinado por cada ítem de dato que llega a este
  nuevo transformador central.
- Se crea un módulo subordinado para el nuevo transformador central.
- Usualmente no debería haber módulos de salida.
- Los nuevos módulos de entrada se factorizan de la misma manera hasta
  llegar a la entrada física.

### Metodología de diseño estructurado 4.2: Factorizar los módulos de salida
La factorización de la salida es simétrica.
- Módulos subordinados: un transformador y módulos de salida.
- Usualmente no debería haber módulos de entrada.

### Metodología de diseño estructurado 4.3: Factorizar los transformadores centrales

- No hay reglas para factorizar los módulos transformadores.
- Utilizar proceso de refinamiento top-down.
- Objetivo: determinar los subtransformadores que compuestos conforman el transformador.
- Repetir el proceso para los nuevos transformadores encontrados.
- Tratar al transformador como un nuevo problema en sí mismo.
- Graficar DFD.
- Luego repetir el proceso de factorización.
- Repetir hasta alcanzar los módulos atómicos.


### Metodología de diseño estructurado 5: Heuristicas del diseño

- Los pasos anteriores no deberían seguirse ciegamente.
- Utilizar heurísticas de diseño para modificar el diseño inicial.
- Tamaño del módulo: Indicador de la complejidad del módulo.
    - Examinar cuidadosamente los módulos con muy poquitas líneas o con mas de 100 líneas.
- Cantidad de flechas de salida y cantidad de flechas de llegada:
    - La primera no debería exceder las 5 o 6 flechas; la segunda debería maximizarse.

### Verificación del diseño

- El objetivo principal es asegurar que el diseño implemente los requerimentos
(corrección).
- También debe realizarse un analisis de desempeño. De todas maneras, la revision
del diseño es la forma más común de realizar la verificacion.
- La calidad del diseño se completa con una buena modularidad.

### Métricas
Estas metricas proveen una evaluacion cuantitativa del diseño.
El tamaño es siempre una metrica. Luego, el diseño se puede estimar mejor.
Otra metrica de interes es la Complejidad.

#### Metricas de Red
Se enfoca en la estructura del diagrama de estructuras, se considera un buen
diagrama aquel en el cual cada modulo tiene un solo modulo invocador.

Cuanto mas se desvie de esta forma de Arbol, más impuro es el diagrama:
(Sea n = nodos del grafo, e = aristas del grafo)

- Impureza del grafo: n - e - 1
- Impureza = 0  => árbol

Esta medida no tiene en cuenta el uso comun de rutinas.

#### Metricas de estabilidad
La estabilidad trata de capturar el impacto de los cambios en el diseño.
La estabilidad de un modulo se define como la cantidad de suposiciones por otros
módulos sobre este. (Se conocen luego del diseño).

#### Metricas de flujo de información
Pero el acoplamiento tambien se incrementa con la complejidad de la interfaz.
Las metricas de flujo de informacion tienen en cuenta:



- La **complejidad intra-módulo**, que se estima con el tamaño del modulo en LOC.
- La **complejidad inter-módulo** que se estima con
  - ``inflow``: flujo de informacion entrante al modulo.
  - ``outflow``: flujo de informacion saliente del modulo.

La complejidad del diseño del modulo C se define como

```
DC = tamaño * (inflow * outflow)<sup>2</sup>
```


La métrica anterior define la complejidad sólo en la cantidad de
información que fluye hacia adentro y hacia fuera y el tamaño del
módulo.
La complejidad del diseño del módulo C se puede definir como:
```
DC = fan_in * fan_out + inflow * outflow
```
donde `fan_in` representa la cantidad de módulos que llaman al módulo C, y `fan_out` los llamados por C.

#### Cómo usar esta métrica

Propenso a error si

```
DC > complej_media + desv_std
```

Complejo si

```
complej_media < DC < complej_media + desv_std
```

Normal en caso contrario.

<div style="page-break-after: always;"></div>


## (!!!) Diseño Orientado a Objetos

### Definición de Acoplamiento
El acoplamiento es un concepto inter-modular, captura la fuerza de interconexión entre módulos.

Tres tipos de acoplamiento: por interacción, de componentes, de herencia

#### Acoplamiento por interacción
Ocurre debido a métodos de una clase que invocan a métodos de otra clase.

Mayor acoplamiento si:

- los métodos acceden partes internas de otros métodos.
- los métodos manipulan directamente variables de otras clases.
- la información se pasa a través de variables temporales.

Menor acoplamiento si los métodos se comunican directamente a través de los parámetros:

- con el menor número de parámetros posible.
- pasando la menor cantidad de información posible.
- pasando sólo datos (y no control).

#### Acoplamiento de componentes
Ocurre cuando una clase A tiene variables de otra clase C, en particular,

- si A tiene variables de instancia de tipo C,
- si A tiene parámetros de tipo C, o
- si A tiene un método con variables locales de tipo C.

Cuando A está acoplado con C, también está acoplado con todas sus
subclases.
Menor acoplamiento si las variables de clase C en A son, o bien
atributos, o bien parámetros en un método, i.e. son visibles.


#### Acoplamiento de herencia:

- Dos clases están acopladas si una es subclase de otra.
- Peor forma de acoplamiento si las subclases modifican la signatura de un método o eliminan un método.
- También es malo si, a pesar de mantener la signatura de un método se modifica el comportamiento de éste (debería preservar la pre/postcondición para que no sea malo).
- Menor acoplamiento si la subclase sólo agrega variables de instancia y métodos pero no modifica los existentes en la superclase.


### Definición de Cohesión
La cohesión es un concepto intra-modular; captura cuán relacionado están los elementos de un módulo.

Tres tipos de cohesión:

#### Cohesión de método

- ¿Por qué los elementos están juntos en el mismo método? (Similar a la cohesión en diseño orientado a funciones.)
- La cohesión es mayor si cada método implementa una única función claramente definida con todos sus elementos contribuyendo a implementar esta función.
- Se debería poder describir con una oración simple que es lo que el

#### Cohesión de clase

- ¿Por qué distintos atributos y métodos están en la misma clase?
- Una clase debería representar un único concepto con todos sus elementos contribuyendo a este concepto.
- Si una clase encapsula múltiples conceptos, la clase pierde cohesión.
- Un síntoma de múltiples conceptos se produce cuando los métodos se pueden separar en diversos grupos, cada grupo accediendo a distintos subconjuntos de atributos.

#### Cohesión de la herencia

- ¿Por qué distintas clases están juntas en la misma jerarquía?
- Hay dos razones para definir subclases: generalización-especialización, y reuso.
- La cohesión es más alta si la jerarquía se produce como consecuencia de la generalización-especialización.

### (!!!) Principio abierto-cerrado
```
Las entidades de software deben ser abiertas para extenderlas y cerradas para
modificarlas
```

Hay que permitir extender el código existente pero no modificarlo. Esto minimiza
el riesgo de dañarlo y es positivo para los programadores ya que prefieren
agregar código en lugar de cambiar el existente.

Este ppio (abierto-cerrado) se satisface si se usa apropiadamente herencia y
polimorfismo.

Principio de sustitución de Liskov (implica el abierto-cerrado)
```
Un programa que utiliza un objeto O cn calse C debería permanecer inalterado si
O se reemplaza por cualquier objeto de una subclase de C.
```
